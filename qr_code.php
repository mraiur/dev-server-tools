<?php
$temp_dir = dirname(__FILE__).'/qr_code/temp/';
$sizes = array('small' => 2, 'medium' => 4, 'large' => 6, 'extra-large' => 8);

$text = isset($_POST['text'])?$_POST['text']:'';
$size = intval( isset($_POST['size'])?$_POST['size']:8 );

$output='';
require_once 'qr_code/qrlib.php';

$name = false;

if(count($_POST)>0){
    if($text!==''){
        $name = time().".png";
        $img = QRcode::png($text, $temp_dir.$name , QR_ECLEVEL_L, $size); 
    
        // clean old images
        $temp_files = scandir($temp_dir);
        foreach( $temp_files as $file ){
            if(!is_dir($temp_dir.$file)) {
                if(filemtime($temp_dir.$file) < time() - 30 ){
                    unlink( $temp_dir.$file );
                }
            }
        }
    }
}
?>



<?php require_once "../header.php";  ?>

   <form action="" method="post">
    <div class="row vertical-center-row">
        <div class="jumbotron">
            <?php if( $name ) { ?>
            <img src="qr_code/temp/<?=$name?>" alt="" border="0" />
            <?php } else { ?>
                Image
            <?php } ?>
        </div>
        <div class="form-group">
            <label >Data</label>
            <textarea  class="form-control" rows="3" name="text"><?=$text?></textarea>
        </div>
        <div class="form-group">
            <label >Size</label>
        <div class="input-group">
        <?php foreach($sizes as $name => $row){ ?>
            <div class="radio">
                <label>
                    <input type="radio" name="size" value="<?=$row?>" <?=($size===$row)?'checked="checked"':""?>> <?=$name?>
                </label>
            </div>
            
        <?php } ?>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-default">Generate</button>
            </div>
        </div>
    

    </div>
    </form>
<?php require_once "../footer.php";  ?>