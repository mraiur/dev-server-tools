# Tools #

* Image to base64 string
* Password crypt
* QR code generator

## Convert image to base64 ##

For example to embed icons in css files.

![base64-image.png](https://bitbucket.org/repo/xMnMR9/images/2498956765-base64-image.png)

## Password crypt ##

Most of the times to convert password to md5 hash.

![password-generator.png](https://bitbucket.org/repo/xMnMR9/images/64041061-password-generator.png)

## QR code generator ##

For fast sharing text/links/notes with your or somebody else phone. 

Currently generating a image will clear QR codes older then 5 minutes.

Library from : [PHP QR Code](http://phpqrcode.sourceforge.net/)

![qr-code-generator.png](https://bitbucket.org/repo/xMnMR9/images/1156423283-qr-code-generator.png)