<?php
$pwd = isset($_POST['pwd'])?$_POST['pwd']:'';
$crypt_method = isset($_POST['crypt_method'])?$_POST['crypt_method']:'md5';

$output='';

if(count($_POST)>0){
	if($crypt_method==='md5'){
		$output = md5($pwd);
	}
}
?>
<?php require_once "../header.php";  ?>
<div class="form-group">
    <label for="output">Crypt password output</label>
    <input type="text" class="form-control" readonly="readonly" id="output" value="<?=$output?>">
</div>

<form role="form" action="" method="post">
    <div class="form-group">
        <label for="pwd">Password: typing is visible!</label>
        <input type="text" class="form-control" id="pwd" value="<?=$pwd?>" placeholder="Password for crypt">
    </div>
    <div class="radio">
        <label>
            <input type="radio" name="crypt_method" id="crypt_method1" value="md5" checked>
            MD5
        </label>
    </div>
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-default">Generate</button>
        </div>
    </div>
</form>

<?php require_once "../footer.php";  ?>