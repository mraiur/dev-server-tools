<?php
$base64 = '';
$msg = '';
if(isset($_FILES["img"])){
	if ($_FILES["img"]["error"] > 0)
	{
		$msg = "Error: " . $_FILES["img"]["error"] . "<br>";
	}
	else
	{
		$msg .= "<div>Upload: " . $_FILES["img"]["name"] . "</div>";
		$msg .= "<div>Type: " . $_FILES["img"]["type"] . "</div>";
		$msg .= "<div>Size: " . ($_FILES["img"]["size"] / 1024) . " kB</div>";
		$msg .= "<div>Stored in: " . $_FILES["img"]["tmp_name"]."</div>";

		$base64  = "data:".$_FILES["img"]["type"].";base64,".base64_encode(file_get_contents($_FILES["img"]["tmp_name"]));

	}
}
?>

<?php require_once "../header.php";  ?>
<div class="jumbotron">
    <?php if( $base64 !=='' ) { ?>
        <div style="margin-bottom:10px;">
            <img src="<?=$base64?>" alt="" border="0" />
        </div>
    <?php 
    echo $msg;
    } else { ?>
        Preview Test
    <?php } ?>
</div>
<div class="form-group">
    <label for="output">Base64 output</label>
    <input type="text" class="form-control" readonly="readonly" id="output" value="<?=$base64?>">
</div>

<form role="form" action="" method="post" enctype="multipart/form-data">
    <div class="form-group">
        <label for="img">Image to convert to base64 </label>
        <input type="file" name="img" class="form-control" id="img"  placeholder="Image to convert">
    </div>
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-default">Generate</button>
        </div>
    </div>
</form>

<?php require_once "../footer.php";  ?>